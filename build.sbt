val circeVersion = "0.11.1"
val circeSpireVersion = "0.1.1"
val fs2Version = "1.0.4"
val catsVersion = "1.6.0"
val catsEffectVersion = "1.2.0"
val shapelessVersion = "2.3.3"
val spireVersion = "0.16.1"
val fs2httpVersion = "0.4.1"

lazy val core = (project in file(".")).settings(
  organization := "org.discordscala",
  name         := "core",
  version      := "0.3.0",
  scalaVersion := "2.12.8",
  resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots"),
  ),
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full),
  libraryDependencies ++= Seq(
    "io.circe" %% "circe-core"           % circeVersion,
    "io.circe" %% "circe-generic"        % circeVersion,
    "io.circe" %% "circe-parser"         % circeVersion,
    "io.circe" %% "circe-generic-extras" % circeVersion,
    "io.circe" %% "circe-spire"          % circeSpireVersion,
    "co.fs2" %% "fs2-core"               % fs2Version,
    "org.typelevel" %% "cats-core"       % catsVersion,
    "org.typelevel" %% "cats-free"       % catsVersion,
    "org.typelevel" %% "cats-effect"     % catsEffectVersion,
    "com.chuusai" %% "shapeless"         % shapelessVersion,
    "org.typelevel" %% "spire"           % spireVersion,
    "com.spinoco" %% "fs2-http"          % fs2httpVersion,
  ),
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Ypartial-unification", "-language:higherKinds"),
)
