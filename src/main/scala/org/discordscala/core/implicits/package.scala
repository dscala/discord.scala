package org.discordscala.core

import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, HCursor}
import io.circe.generic.extras.Configuration
import io.circe.spire.SpireCodecs
import io.circe.syntax._
import org.discordscala.core.event.{Event, SimpleEventMatcher}

package object implicits extends SpireCodecs {

  implicit val config: Configuration = Configuration.default.withSnakeCaseMemberNames

  implicit class EventHandlerOps[F[_]](e: EventHandler[F]) {

    def +(o: EventHandler[F]): EventHandler[F] = { in => csr =>
      e(in)(csr) ++ o(in)(csr)
    }

  }

  implicit def betterListDecoder[A: Decoder]: Decoder[List[A]] = Decoder.decodeOption[List[A]](Decoder.decodeList[A]).map(_.getOrElse(List.empty))

}
