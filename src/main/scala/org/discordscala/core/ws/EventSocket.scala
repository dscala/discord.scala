package org.discordscala.core.ws

import cats._
import cats.data._
import cats.implicits._
import cats.effect._
import cats.effect.concurrent.Ref
import fs2.concurrent.Queue
import fs2.{io => _, _}
import io.circe.{Encoder, Json}
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._
import org.discordscala.core.{Client, EventHandler, EventHandlers}
import org.discordscala.core.event.{Event, EventMatcher}
import org.discordscala.core.implicits._
import spinoco.fs2.http.websocket._

case class EventSocket[F[_]](
  client: Client[F],
  myShard: Int,
  maxShards: Int,
  matcher: EventMatcher[F],
  handler: EventHandlers[F]
)(
  implicit flatmap: FlatMap[F],
  concurrent: Concurrent[F]
) {

  def wsPipe: Stream[F, Pipe[F, Frame[String], Frame[String]]] = {
    val seqNum: F[Ref[F, Option[Long]]] = Ref[F].of(None)
    Stream
      .eval(seqNum)
      .map { ref =>
        val m2ref = handler.map(_(ref))
        inbound: Stream[F, Frame[String]] =>
          {
            inbound.map { in =>
              println(s"Received something: ${in.a}")
              parse(in.a).right.get
            }.evalTap { json =>
              println(s"In evalTap for ${json.toString().lines.take(4).toList.mkString("\n")}")
              ref.update { cs =>
                json.asObject.fold(cs) { jo =>
                  jo("s").flatMap(_.asNumber).flatMap(_.toInt).fold(cs)(ni => Some(ni))
                }
              }
            }.flatMap { json =>
              matcher(client, json)
                .handleErrorWith(e => Stream.eval(Sync[F].delay(e.printStackTrace())).drain)
                .map { e =>
                  implicit val de = e.dataEncoder
                  println(s"Received ${e.toString.lines.take(1).toList.head}")
                  e
                }
            }.broadcastThrough(m2ref: _*)
              .flatMap { e =>
                implicit val de: Encoder[e.Data] = e.dataEncoder
                println(s"Sending $e, ${e.d.asJson}")
                EventSocket.websocketFrame(e, ref)
              }
          }
      }
  }

}

object EventSocket {

  def stockEvent[F[_]](e: Event[F], s: Option[Long]): EventStruct[e.Data] = {
    implicit val enc: Encoder[e.Data] = e.dataEncoder
    EventStruct(e.op, e.d, s, e.t)
  }

  def websocketFrame[F[_]](e: Event[F], r: Ref[F, Option[Long]])(
    implicit flatmap: FlatMap[F]
  ): Stream[F, Frame.Text[String]] = {
    Stream.eval(
      r.get.map[Frame.Text[String]](cs => {
        implicit val enc: Encoder[e.Data] = e.dataEncoder
        val se = stockEvent(e, cs)
        Frame.Text(se.asJson.noSpaces)
      })
    )
  }

}

private[ws] case class EventStruct[D: Encoder](op: Int, d: D, s: Option[Long], t: Option[String])
