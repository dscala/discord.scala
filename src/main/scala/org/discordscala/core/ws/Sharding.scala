package org.discordscala.core.ws

import cats.Monad
import cats.effect.Sync
import fs2.Stream
import io.circe.generic.auto._
import org.discordscala.core.Client
import org.discordscala.core.util.RequestUtil

trait Sharding[F[_]] {

  val maxShards: Stream[F, Int]
  val myShards: Stream[F, Int]

}

object Sharding {

  case class BotGatewayResponse(shards: Int)

  def recommendedShards[F[_]](implicit client: Client[F], monad: Monad[F], sync: Sync[F]): Stream[F, Int] = {
    val c = RequestUtil.get[F, BotGatewayResponse]("gateway/bot")
    c.map(_.shards)
  }

  case class Max[F[_]]()(implicit client: Client[F], monad: Monad[F], sync: Sync[F]) extends Sharding[F] {

    override val maxShards: Stream[F, Int] = recommendedShards
    override val myShards: Stream[F, Int] = maxShards.flatMap(max => Stream.emits(0 until max))

  }

  case class Single[F[_]]() extends Sharding[F] {

    override val maxShards: Stream[F, Int] = Stream(1)
    override val myShards: Stream[F, Int] = Stream(0)

  }

  case class One[F[_]](max: Int, which: Int) extends Sharding[F] {

    override val maxShards: Stream[F, Int] = Stream(max)
    override val myShards: Stream[F, Int] = Stream(which)

  }

}
