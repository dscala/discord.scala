package org.discordscala

import java.util.concurrent.TimeUnit

import cats.Functor
import cats.effect.Timer
import cats.effect.concurrent.Ref
import fs2.{Pipe, RaiseThrowable, Stream}
import org.discordscala.core.event.{Event, SimpleEventMatcher}
import org.discordscala.core.event.inbound.{Hello, HelloData, MessageCreate, PresenceUpdate, PresenceUpdateData}
import org.discordscala.core.implicits._
import io.circe.generic.auto._
import org.discordscala.core.event.neutral.Heartbeat
import org.discordscala.core.event.outbound.Identify
import org.discordscala.core.model.Message
import spire.math.ULong

import scala.concurrent.duration.FiniteDuration

package object core {

  def defaultEvents[F[_]: RaiseThrowable]: SimpleEventMatcher[F] =
    SimpleEventMatcher[F](c => {
      case (0, d, s, Some("MESSAGE_CREATE")) =>
        println("hi")
        val am = d.as[Message]
        println(am)
        MessageCreate(c, am.right.get)
      case (0, d, s, Some("PRESENCE_UPDATE")) => PresenceUpdate(c, d.as[PresenceUpdateData].right.get)
      case (10, d, s, None) => Hello(c, d.as[HelloData].right.get)
//      case (11, j, s, None) => // TODO Heartbeat Ack
//      case (1, d, s, None) => // TODO Heartbeat
    })

  def defaultHandler[F[_]: Timer: Functor]: EventHandler[F] = { ref => in =>
    in.flatMap {
      case Hello(c, HelloData(interval)) =>
        Stream(Identify(c)) ++ (Stream(FiniteDuration(0, TimeUnit.MILLISECONDS)) ++ Stream.awakeEvery[F](
          FiniteDuration(interval, TimeUnit.MILLISECONDS)
        )).flatMap { _ =>
          Stream
            .eval(ref.get)
            .map { cs =>
              Heartbeat(c, cs)
            }
        }
      case _ => Stream.empty
    }
  }

  type EventHandler[F[_]] = Ref[F, Option[Long]] => Pipe[F, Event[F], Event[F]]
  type EventHandlers[F[_]] = List[EventHandler[F]]

  object SimpleEventHandler {

    def apply[F[_]](func: Event[F] => Stream[F, Event[F]]): EventHandler[F] = { _ => in => in.flatMap(func) }

  }

  type Snowflake = ULong

}
