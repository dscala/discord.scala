package org.discordscala.core.util

import cats._
import cats.effect._
import cats.implicits._
import fs2.{RaiseThrowable, Stream}
import io.circe._
import io.circe.parser._
import org.discordscala.core.Client
import scodec.{Attempt, Err}
import scodec.bits.ByteVector
import spinoco.fs2.http.body.BodyDecoder
import spinoco.fs2.http.{util, HttpClient, HttpRequest, HttpResponse}
import spinoco.fs2.http.body.BodyDecoder.stringDecoder
import spinoco.protocol.http.HttpStatusCode
import spinoco.protocol.http.header.value.HttpCredentials
import spinoco.protocol.http.header.{Authorization, HttpHeader}
import spinoco.protocol.mime.{ContentType, MIMECharset}

import scala.util.{Failure, Success, Try}

object RequestUtil {

  def authHeader[F[_]](implicit client: Client[F]): HttpHeader =
    Authorization(HttpCredentials.OAuthToken("Bot", client.token))

  def get[F[_], A](
    path: String,
    headers: List[HttpHeader] = Nil
  )(implicit client: Client[F], decoder: Decoder[A], monad: Monad[F], sync: Sync[F]): Stream[F, A] = {
    val request =
      HttpRequest.get[F](client.apiRoot.copy(path = client.apiRoot.path / path)).withHeader(authHeader, headers: _*)
    ratelimited[F, A](request)
  }

  def ratelimited[F[_], A](
    request: HttpRequest[F]
  )(implicit client: Client[F], decoder: Decoder[A], monad: Monad[F], sync: Sync[F]): Stream[F, A] = {
    val fH: F[HttpClient[F]] = client.httpClient
    val parsed: F[Stream[F, Try[A]]] = fH.map(c => {
      val iResp: Stream[F, HttpResponse[F]] = responseFromClientRequest(c, request)
      val hResp: Stream[F, Try[HttpResponse[F]]] = iResp.map(handleErrors)
      val sResp: Stream[F, Try[String]] = hResp.flatMap(_.traverse(stringBodyFromResponse(_)).map(_.flatten))
      val jResp: Stream[F, Try[Json]] = sResp.map(_.flatMap(jsonFromStringBody))
      val aResp: Stream[F, Try[A]] = jResp.map(_.flatMap(aFromJson[A]))
      aResp
    })
    Stream.eval(parsed).flatten.flatMap(_.fold(Stream.raiseError[F](_), Stream.emit))
  }

  def responseFromClientRequest[F[_]](c: HttpClient[F], request: HttpRequest[F]): Stream[F, HttpResponse[F]] = {
    c.request(request)
    // TODO handle ratelimiting here
  }

  def handleErrors[F[_]](resp: HttpResponse[F]): Try[HttpResponse[F]] = {
    resp.header.status match {
      case HttpStatusCode.Ok => Success(resp)
      case err => Failure(new IllegalStateException(s"Error status code: $err"))
    }
  }

  def stringBodyFromResponse[F[_]](resp: HttpResponse[F])(implicit sync: Sync[F]): Stream[F, Try[String]] = {
    implicit val sd: BodyDecoder[String] = (bytes: ByteVector, contentType: ContentType) => {
      MIMECharset.asJavaCharset(util.getCharset(contentType).getOrElse(MIMECharset.`UTF-8`)).flatMap { implicit chs =>
        Attempt.fromEither(
          bytes.decodeString.left
            .map(ex => Err(s"Failed to decode string ContentType: $contentType, charset: $chs, err: ${ex.getMessage}"))
        )
      }
    }
    Stream
      .eval(resp.bodyAs[String])
      .map(_.toEither.leftMap(e => new IllegalStateException(s"Could not get a string body: $e")).toTry)
  }

  def jsonFromStringBody(str: String): Try[Json] = {
    parse(str).toTry
  }

  def aFromJson[A](json: Json)(implicit decoder: Decoder[A]): Try[A] = {
    json.as[A].toTry
  }

}
