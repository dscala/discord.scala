package org.discordscala.core.event
import io.circe.Encoder
import org.discordscala.core.Client

trait Event[F[_]] {

  type Data
  val dataEncoder: Encoder[Data]

  val op: Int
  val d: Data
  val t: Option[String]

  @transient val c: Client[F]

}
