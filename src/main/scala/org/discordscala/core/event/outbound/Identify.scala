package org.discordscala.core.event.outbound

import io.circe.Encoder
import io.circe.generic.auto._
import io.circe.generic.extras.ConfiguredJsonCodec
import org.discordscala.core.Client
import org.discordscala.core.event.Event
import org.discordscala.core.implicits._

case class Identify[F[_]](c: Client[F]) extends Event[F] {

  override val dataEncoder: Encoder[IdentifyData] = implicitly[Encoder[IdentifyData]]

  override val d: IdentifyData = IdentifyData(c.token)
  override val op: Int = 2
  override val t: Option[String] = None

  override type Data = IdentifyData

}

@ConfiguredJsonCodec case class IdentifyData(token: String, properties: ConnectionProperties = ConnectionProperties())

case class ConnectionProperties(
  `$os`: String = System.getProperty("os.name"),
  `$browser`: String = "discord.scala",
  `$device`: String = "discord.scala"
)
