package org.discordscala.core.event.neutral

import io.circe._
import io.circe.syntax._
import org.discordscala.core.Client
import org.discordscala.core.event.Event

case class Heartbeat[F[_]](c: Client[F], d: Option[Long]) extends Event[F] {

  override val dataEncoder: Encoder[Option[Long]] = implicitly[Encoder[Option[Long]]]

  override val op: Int = 1
  override val t: Option[String] = None

  override type Data = Option[Long]

}
