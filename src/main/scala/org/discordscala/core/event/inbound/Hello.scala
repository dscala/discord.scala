package org.discordscala.core.event.inbound

import io.circe.Encoder
import org.discordscala.core.event.Event
import org.discordscala.core.implicits._
import io.circe.generic.auto._
import io.circe.generic.extras.ConfiguredJsonCodec
import org.discordscala.core.Client

case class Hello[F[_]](c: Client[F], d: HelloData) extends Event[F] {

  override val dataEncoder: Encoder[HelloData] = implicitly[Encoder[HelloData]]

  val op: Int = 10
  val t: Option[String] = None

  override type Data = HelloData

}

@ConfiguredJsonCodec case class HelloData(heartbeatInterval: Long)
