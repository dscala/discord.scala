package org.discordscala.core.event.inbound

import io.circe.Encoder
import io.circe.generic.auto._
import org.discordscala.core.Client
import org.discordscala.core.event.Event
import org.discordscala.core.model.Message

case class MessageCreate[F[_]](c: Client[F], d: Message) extends Event[F] {

  override val dataEncoder: Encoder[Message] = implicitly[Encoder[Message]]

  override  val op: Int = 0
  override  val t: Option[String] = Some("MESSAGE_CREATE")

  override type Data = Message

}
