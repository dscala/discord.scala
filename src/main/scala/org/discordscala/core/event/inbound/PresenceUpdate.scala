package org.discordscala.core.event.inbound

import io.circe.{Encoder, Json}
import io.circe.generic.auto._
import io.circe.generic.extras.ConfiguredJsonCodec
import org.discordscala.core.event.Event
import org.discordscala.core.{Client, Snowflake}
import org.discordscala.core.implicits._

case class PresenceUpdate[F[_]](c: Client[F], d: PresenceUpdateData) extends Event[F] {

  override val dataEncoder: Encoder[Data] = implicitly[Encoder[PresenceUpdateData]]

  val op: Int = 0
  val t: Option[String] = Some("PRESENCE_UPDATE")

  override type Data = PresenceUpdateData

}

@ConfiguredJsonCodec case class PresenceUpdateData(
  user: Json, // TODO
  roles: List[Snowflake],
  game: Option[Json],
  guildId: Snowflake,
  status: String, // TODO
  activities: List[Json], // TODO
  clientStatus: ClientStatus
)

@ConfiguredJsonCodec case class ClientStatus(
  desktop: Option[String], // TODO
  mobile: Option[String], // TODO
  web: Option[String] // TODO
)
