package org.discordscala.core.event

import fs2.{RaiseThrowable, Stream}
import io.circe.Json
import org.discordscala.core.Client

import scala.util.{Failure, Success, Try}

trait EventMatcher[F[_]] {

  def apply(c: Client[F], op: Int, d: Json, s: Option[Int], t: Option[String]): Stream[F, Event[F]]

  def apply(c: Client[F], j: Json): Stream[F, Event[F]] =
    Stream {
      val json = j.asObject.get
      val op = json("op").flatMap(_.asNumber).flatMap(_.toInt).get
      val d = json("d").get
      val s = json("s").flatMap(_.asNumber).flatMap(_.toInt)
      val t = json("t").flatMap(_.asString)
      apply(c, op, d, s, t)
    }.flatten

  final def +(other: EventMatcher[F]): Stream[F, EventMatcher[F]] =
    Stream[F, EventMatcher[F]](
      (c: Client[F], op: Int, d: Json, s: Option[Int], t: Option[String]) =>
        EventMatcher.this.apply(c, op, d, s, t) ++ other.apply(c, op, d, s, t)
    )

}

case class SimpleEventMatcher[F[_]: RaiseThrowable](
  m: Client[F] => PartialFunction[(Int, Json, Option[Int], Option[String]), Event[F]]
) extends EventMatcher[F] {

  override def apply(c: Client[F], op: Int, d: Json, s: Option[Int], t: Option[String]): Stream[F, Event[F]] = {
    Stream(Try(m(c).lift(op, d, s, t)))
      .map {
        case Success(Some(e)) => Success(e)
        case Success(None) => Failure(new IllegalArgumentException("Unknown event received!"))
        case Failure(f) => Failure(f)
      }
      .map(e => {
        println(s"Matched $e")
        e
      })
      .flatMap(_.fold(Stream.raiseError[F](_), Stream.emit))
  }

}
