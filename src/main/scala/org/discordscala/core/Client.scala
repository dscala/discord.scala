package org.discordscala.core

import java.nio.channels.AsynchronousChannelGroup

import cats._
import cats.data._
import cats.effect.concurrent.Ref
import cats.implicits._
import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import fs2.Stream
import fs2.Pipe
import fs2.concurrent.Queue
import io.circe.{Decoder, Json}
import org.discordscala.core.event.{Event, EventMatcher}
import org.discordscala.core.ws.{EventSocket, Sharding}
import scodec.codecs.implicits._
import scodec.codecs.utf8
import spinoco.fs2.http
import spinoco.fs2.http.HttpClient
import spinoco.fs2.http.websocket.{Frame, WebSocketRequest}
import spinoco.protocol.http.{HttpResponseHeader, Uri}

case class Client[F[_]](
  token: String,
  apiRoot: Uri = Uri.https("discordapp.com", "/api/v6/"),
  gatewayRoot: Uri = Uri.wss("gateway.discord.gg", "/?v=6&encoding=json"),
)(
  implicit monad: Monad[F],
  cce: ConcurrentEffect[F],
  ctxShift: ContextShift[F],
  timer: Timer[F],
  ag: AsynchronousChannelGroup
) {

  val httpClient: F[HttpClient[F]] = http.client[F]()

  // Both have to be set for the functionality to take place
  val replaceInputStream: Option[Stream[F, Frame[String]]] = None // @mpilquist
  val replaceOutputPipe: Option[Pipe[F, Frame[String], Unit]] = None // @mpilquist

  def login(
    handler: EventHandlers[F],
    eventMatcher: EventMatcher[F] = defaultEvents[F],
    sharding: Sharding[F] = Sharding.Max()(this, monad, cce)
  ): Stream[F, Unit /* Option[HttpResponseHeader] */] = {
    Stream
      .eval(httpClient)
      .flatMap(client => {
        val req = gatewayRoot.host.port match {
          case Some(port) => WebSocketRequest.wss(gatewayRoot.host.host, port, gatewayRoot.path.stringify)
          case None => WebSocketRequest.wss(gatewayRoot.host.host, gatewayRoot.path.stringify)
        }
        val esS = for {
          myS <- sharding.myShards
          maxS <- sharding.maxShards
          es <- EventSocket(this, myS, maxS, eventMatcher, handler).wsPipe
        } yield es
        esS.flatMap(pipe => {

          (replaceInputStream, replaceOutputPipe) match {
            case (Some(i), Some(p)) => i.through(pipe).through(p)
            case _ => client.websocket(req, pipe)(utf8, utf8).map(_ => ())
          }

        })
      })
  }

}
