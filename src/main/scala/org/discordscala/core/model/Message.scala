package org.discordscala.core.model

import io.circe.Json
import io.circe.generic.auto._
import io.circe.generic.extras.ConfiguredJsonCodec
import org.discordscala.core.Snowflake
import org.discordscala.core.implicits._
import spire.math.ULong

@ConfiguredJsonCodec case class Message(
  id: Snowflake,
  channelId: Snowflake,
  guildId: Option[Snowflake],
  author: Json, // TODO
  member: Option[Json],
  content: String,
  timestamp: String, // TODO
  editedTimestamp: Option[String], // TODO
  tts: Boolean,
  mentionEveryone: Boolean,
  mentions: List[Json], // TODO
  mentionRoles: List[Snowflake],
  attachments: List[Json], // TODO
  embeds: List[Json], // TODO
  reactions: List[Json], // TODO
  nonce: Option[Snowflake],
  pinned: Boolean,
  webhookId: Option[Snowflake],
  `type`: Int, // TODO
  activity: Option[Json], // TODO
  application: Option[Json] // TODO
)
