package tf.bug.dscalatest

import cats._
import cats.implicits._
import cats.effect._
import fs2._
import java.nio.channels.AsynchronousChannelGroup
import java.util.concurrent.Executors
import org.discordscala.{ core => cord }
import cord.Client
import cord.SimpleEventHandler
import cord.event.Event
import cord.event.inbound.MessageCreate

object Test extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    Program[IO].compile.drain.as(ExitCode.Success)
  }

}

object Program {

  implicit val ag: AsynchronousChannelGroup =
    AsynchronousChannelGroup.withThreadPool(Executors.newCachedThreadPool())

  def apply[F[_]: ConcurrentEffect: ContextShift: Timer]: Stream[F, Unit] = {
    val client = Client[F](System.getenv("DISCORD_BOT_TOKEN"))
    client.login(
      cord.defaultHandler :: SimpleEventHandler { e: Event[F] =>
        e match {
          case MessageCreate(_, d) =>
            println(d.content)
            Stream.empty
          case _ => Stream.empty
        }
      } :: Nil
    ).drain
  }

}
